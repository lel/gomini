package gomini

import (
	"bufio"
	"bytes"
	"crypto/tls"
	"errors"
	"io/ioutil"
	"net/url"
	"strings"
)

// This file defines the basic behavior of Requests and Responses as necessary to
// allow Getting of files from Gemini servers.

// Fetch builds a request from a URI, then handles it and produces a Response;
// basically wraps all of this together more neatly.
func Fetch(uri string) (Response, error) {
	req, err := BuildRequest(uri)
	if err != nil {
		return *new(Response), err
	}

	resp, err := req.Get()
	if err != nil {
		return *new(Response), err
	}
	return resp, nil
}

// Request extends url.URL to handle gemini requests, etc.
type Request struct {
	url.URL
}

// Response contains the various parts of a response from a Gemini server.
type Response struct {
	URL  Request
	Mime Mime
	Body []byte
}

// BuildRequest wraps url.Parse to parse a url string to produce a gemini Request.
func BuildRequest(s string) (Request, error) {
	uri, err := url.Parse(s)
	if err != nil {
		return *new(Request), err
	}
	// who wants to type "gemini://" every time?
	if uri.Scheme == "" {
		uri.Scheme = "gemini"
		// lol
		uri, err = url.Parse(uri.String())
		if err != nil {
			return *new(Request), err
		}
	}
	// but now the scheme wasn't empty and is something different so shut this down
	if uri.Scheme != "gemini" {
		return *new(Request), errors.New("bad scheme")
	}
	// should have port 1965 but an empty one is fine
	if uri.Port() == "" {
		uri.Host += ":1965"
	}
	return Request{*uri}, nil
}

// Request.Get wraps Request.GetRaw to dial the server associated with a Request and
// produce a Response, handling redirects.
func (req Request) Get() (Response, error) {
	// loops 5 times to not get redirect trapped
	for i := 0; i < 5; i++ {
		// gets low-level response
		rawResponse, err := req.GetRaw()
		if err != nil {
			return *new(Response), err
		}
		// checks for redirect
		if !LooksLikeRedirect(rawResponse) {
			// otherwise, wrap everything up
			response, err := NewResponse(rawResponse)
			if err != nil {
				return *new(Response), err
			}
			response.URL = req
			return response, nil
		}
		// but if so handle it and loop back
		req, err = ParseRedirect(rawResponse)
		if err != nil {
			return *new(Response), err
		}
		// fmt.Printf("redirecting to %s\n", req.String())
	}
	// we did our best
	return *new(Response), errors.New("redirect loop")
}

// LooksLikeRedirect returns whether or not a raw response matches the format of a redirect.
func LooksLikeRedirect(rawResponse []byte) bool {
	// this might be naïve, idk
	if len(rawResponse) > 2 && (string(rawResponse[0:2]) == "31" || string(rawResponse[0:2]) == "30") {
		return true
	}
	return false
}

// ParseRedirect returns a request from a raw response.
func ParseRedirect(rawResponse []byte) (Request, error) {
	if len(rawResponse) < 4 {
		return *new(Request), errors.New("invalid redirect")
	}
	redirection := rawResponse[3:]
	toTrim := []string{"\x00", "\n", "\r"}
	for _, garbage := range toTrim {
		redirection = bytes.Trim(redirection, garbage)
	}
	return BuildRequest(string(redirection))
}

// Request.GetRaw dials the server associated with a Request and returns a raw byte-slice response.
func (req Request) GetRaw() ([]byte, error) {
	// first of all
	if req.Scheme != "gemini" {
		return nil, errors.New("bad scheme")
	}
	// connect
	conn, err := Dial(req.Host)
	if err != nil {
		return nil, err
	}
	defer conn.Close()
	// send our Request
	_, err = conn.Write([]byte(req.String() + "\r\n"))
	if err != nil {
		return nil, err
	}
	// read a (byte-slice) response to our Request
	responseReader := bufio.NewReader(conn)
	rawResponse, err := ioutil.ReadAll(responseReader)
	if err != nil {
		return nil, err
	}
	return rawResponse, nil
}

// Dial wraps tls.Dial to Dial a gemini server.
func Dial(host string) (*tls.Conn, error) {
	conf := &tls.Config{InsecureSkipVerify: true}
	return tls.Dial("tcp", host, conf)
}

// NewResponse takes a server byte-slice response and produces a Response (sin Request).
func NewResponse(rawResponse []byte) (Response, error) {
	mimeStruct, err := GetMime(rawResponse)
	if err != nil {
		return *new(Response), err
	}
	stringResponse, err := DeMime(rawResponse)
	if err != nil {
		return *new(Response), err
	}
	return Response{*new(Request), mimeStruct, stringResponse}, nil
}

// Splits a Response body into lines at newline characters.
func (resp Response) LineBreak() []string {
	return strings.Split(string(resp.Body), "\n")
}
