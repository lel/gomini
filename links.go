package gomini

import (
	"errors"
	"fmt"
	"net/url"
	"sort"
	"strings"
)

// Link holds a name and a gemini Request to represent a Gemini link line.
type Link struct {
	Description string
	Destination Request
}

// ParseLink turns the string for a link into a Link with respect to a location.
func ParseLink(s string, location Request) (Link, error) {
	// lol
	href, err := getLinkPieces(s)
	if err != nil {
		return *new(Link), err
	}
	name := href[1]
	rel, err := url.Parse(href[0])
	if err != nil {
		return *new(Link), err
	}
	dest, err := BuildRequest(location.ResolveReference(rel).String())
	return Link{name, dest}, err
}

// LooksLikeLink returns whether a line looks like it could be turned into a Link.
func LooksLikeLink(line string) bool {
	// maybe naïve
	if len(line) > 2 && line[0:2] == "=>" {
		return true
	}
	return false
}

// Produces the relevant bits of a Link from a link line.
func getLinkPieces(line string) ([]string, error) {
	line = Respace(line)
	href := strings.SplitN(line[3:], " ", 2)
	if len(href) == 2 {
		return href, nil
	}
	// Maybe it's one of those annoying links with no text
	href = strings.SplitN(line, " ", 2)
	if len(href) == 2 {
		return []string{href[1], href[1]}, nil
	}
	return []string{}, errors.New("malformed link")
}

// Respace returns a string with all whitespace nonsense that gemini.conman.org
// decides to throw my way torn out and replaced with spaces.
func Respace(line string) string {
	return strings.Join(strings.Fields(line), " ")
}

// Link.Follow returns a response and an error.
func (link Link) Follow(resp *Response) (Response, error) {
	return link.Destination.Get()
}

// Link.String() implements Stringer.
func (link Link) String() string {
	return fmt.Sprintf("%s -> %s", link.Description, link.Destination)
}

// Index is a set of Lines mapped to ints representing line numbers.
type Index map[int]Link

// Index.At returns a link from a particular line, or an error if no link exists for that line.
func (ind Index) At(n int) (Link, error) {
	for line, link := range ind {
		if n == line {
			return link, nil
		}
	}
	return *new(Link), errors.New("no such link")
}

// Response.String() implements Stringer. This isn't necessarily how you'll want to print it in
// a client implementation or something but it was good for debugging; w/e.
func (resp Response) String() string {
	if !resp.IsText() {
		return string(resp.Body)
	}
	index := resp.Index()
	lines := resp.LineBreak()
	// this idiom is how i've been getting an ordered key to the index
	var key []int
	for n := range index {
		key = append(key, n)
	}
	sort.Ints(key)
	for i, line := range key {
		// format links a bit y'know
		lines[line] = fmt.Sprintf("[%v] %s", i, index[line].Description)
	}
	return strings.Join(lines, "\n")
}

// Response.Index() produces an Index from a Response.
func (resp Response) Index() Index {
	if !resp.IsText() {
		return nil
	}
	lines := resp.LineBreak()
	ind := make(Index)
	for i, line := range lines {
		if !LooksLikeLink(line) {
			continue
		}
		newLink, err := ParseLink(line, resp.URL)
		if err != nil {
			continue
		}
		ind[i] = newLink
	}
	return ind
}

// Index.ByNumber produces a link (or error) from an int representing the link number.
func (index Index) ByNumber(n int) (Link, error) {
	if n >= len(index) {
		return *new(Link), errors.New("no such link")
	}
	var key []int
	for line := range index {
		key = append(key, line)
	}
	sort.Ints(key)
	return index[key[n]], nil
}

// Response.FollowLink returns a Response from an int corresponding to a link number in the current Response.
func (resp Response) FollowLink(n int) (Response, error) {
	index := resp.Index()
	if len(index) < n {
		return *new(Response), errors.New("no such link")
	}
        var key []int
        for line := range index {
                key = append(key, line)
        }
        sort.Ints(key)

        link := index[key[n]]
	return link.Follow(&resp)
}
