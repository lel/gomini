package gomini

import (
	"errors"
	"mime"
	"strings"
)

type Mime struct {
	Type   string
	Params map[string]string
}

// GetMime takes a raw response from a server and returns its mime type as
// a string, along with mime params and possible errors along the way.
func GetMime(response []byte) (Mime, error) {
	mimeString := strings.SplitN(string(response), "\n", 2)[0]
	mimeSlice := strings.SplitN(mimeString, "\t", 2)
	if len(mimeSlice) < 2 {
		return *new(Mime), errors.New("mimeless")
	}
	mimeString = mimeSlice[1]
	mimeType, params, err := mime.ParseMediaType(mimeString)
	return Mime{mimeType, params}, err
}

// DeMime takes a Gemini response as a string and removes the mime header from it,
// returning what's left.
func DeMime(response []byte) ([]byte, error) {
	pageSplit := strings.SplitN(string(response), "\n", 2)
	if len(pageSplit) < 2 {
		return nil, errors.New("invalid response")
	}
	return []byte(pageSplit[1]), nil
}

// Response.IsText returns whether a response is a text type or not.
func (resp Response) IsText() bool {
	return resp.Mime.Type == "text/gemini" || resp.Mime.Type == "text/raw"
}
